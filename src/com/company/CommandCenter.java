package com.company;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {
    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> availableSpaceships = new ArrayList<>();
        int indexNow = -1;

        for (int i = 0; i < ships.size(); i++) {
            if (availableSpaceships.size() == 0) {
                if (ships.get(i).getFirePower() > 0) {
                    availableSpaceships.add(ships.get(i));
                    indexNow++;
                }
            } else {
                if (ships.get(i).getFirePower() > availableSpaceships.get(indexNow).getFirePower()) {
                    availableSpaceships.add(ships.get(i));
                    indexNow++;
                }
            }
        }

        if (availableSpaceships.size() == 0) {
            return null;
        } else {
            return availableSpaceships.get(availableSpaceships.size() - 1);
        }
    }


    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        Spaceship shipWithSameName = null;

        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getName().equals(name)){
                shipWithSameName = ships.get(i);
                break;
            }
        }
        return shipWithSameName;
    }
    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> bigSpaceship = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getCargoSpace()>=cargoSize){
                bigSpaceship.add(ships.get(i));
            }
        }
        return bigSpaceship;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> friendlySpaceship = new ArrayList<>();
        for (int i = 0; i < ships.size(); i++) {
            if (ships.get(i).getFirePower()==0){
                friendlySpaceship.add(ships.get(i));
            }
        }
        return friendlySpaceship;
    }
}
