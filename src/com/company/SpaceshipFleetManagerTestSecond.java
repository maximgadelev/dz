package com.company;
import org.junit.jupiter.api.*;
import java.util.ArrayList;

public class SpaceshipFleetManagerTestSecond {
    static SpaceshipFleetManager center = new CommandCenter();
    static ArrayList<Spaceship> testShips = new ArrayList<>();
    static boolean flag = true;

    @AfterEach
    void clearAllShips() {
        flag = true;
        testShips.clear();
    }

    @DisplayName("Вернуть корабль с самой большой огневой мощью")
    @Test
    void getFirePower_BiggestPower_returnPowerfullShip() {
        int testPower = 10;
        testShips.add(new Spaceship("Big", testPower, 1, 1));
        testShips.add(new Spaceship("Small", 0, 1, 1));
        testShips.add(new Spaceship("Mid", 0, 1, 1));
        Spaceship result = center.getMostPowerfulShip(testShips);
        Assertions.assertEquals(testPower, result.getFirePower());
    }

    @DisplayName("Возратить null,если нет самого мощного корабля")
    @Test
    void getFirePower_NotBiggestPower_returnNull() {
        testShips.add(new Spaceship("Big", 0, 1, 1));
        testShips.add(new Spaceship("Small", -1, 1, 1));
        testShips.add(new Spaceship("Mid", -1, 1, 1));
        Spaceship result = center.getMostPowerfulShip(testShips);
        Assertions.assertNull(result);
    }

    @DisplayName("Вернуть корабль первый по списку")
    @Test
    void getFirePower_BiggestPower_returnPowerFullShipFirstInList() {
        int testPower = 10;
        String testName = "Test ship";
        testShips.add(new Spaceship(testName, testPower, 1, 1));
        testShips.add(new Spaceship("Big", 10, 1, 1));
        testShips.add(new Spaceship("Small", 10, 1, 1));
        testShips.add(new Spaceship("Mid", 0, 1, 1));
        Spaceship result = center.getMostPowerfulShip(testShips);
        if (result.getFirePower() != 10 || !result.getName().equals(testName)) {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("Вернуть корабль с уникальным именем")
    @Test
    void getShipByName_shipName_returnShipName() {
        testShips.add(new Spaceship("Big", 1, 1, 1));
        testShips.add(new Spaceship("Small", 1, 1, 1));
        testShips.add(new Spaceship("Mid", 1, 1, 1));
        String name = "Mid";
        Spaceship result = center.getShipByName(testShips, name);
        if (result == null || !result.getName().equals(name)) {
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("Вернуть нул,если корабля с таким именем не нашлось")
    @Test
    void getShip_NotExist_sayNull() {
        testShips.add(new Spaceship("Big", 1, 1, 1));
        testShips.add(new Spaceship("Small", 1, 1, 1));
        testShips.add(new Spaceship("Mid", 1, 1, 1));
        String name = "Mid123";
        Spaceship result = center.getShipByName(testShips, name);
        Assertions.assertNull(result);
    }

    @DisplayName("Вернуть корабли с достаточной вместимостью")
    @Test
    void getAllShipsWithEnoughCargoSpace_shipsListWithEnoughCargoSpace_returnListWithShips() {
        testShips.add(new Spaceship("Big", 0, 5, 1));
        testShips.add(new Spaceship("Small", -1, 9, 1));
        testShips.add(new Spaceship("Mid", -1, 10, 1));
        int testCargo = 10;
        ArrayList<Spaceship> testCargoSpaceships = center.getAllShipsWithEnoughCargoSpace(testShips, testCargo);
        if (testCargoSpaceships.size() == 0) {
            flag = false;
        }
        for (int i = 0; i < testCargoSpaceships.size(); i++) {
            if (testCargoSpaceships.get(i).getCargoSpace() < testCargo) {
                flag = false;
            }
        }
        Assertions.assertTrue(flag);
    }

    @DisplayName("Вернуть пустой список,если нет кораблей с подходящей вместимостью")
    @Test
    void getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnEmptyList() {
        testShips.add(new Spaceship("Big", 0, 9, 1));
        testShips.add(new Spaceship("Small", -1, 8, 1));
        testShips.add(new Spaceship("Mid", -1, 10, 1));
        int testCargo = 1320;
        ArrayList<Spaceship> testCargoSpaceships = center.getAllShipsWithEnoughCargoSpace(testShips, testCargo);
        Assertions.assertEquals(testCargoSpaceships.size(), 0);
    }
    @DisplayName("Возвратить мирные кораблики")
    @Test
    void getAllCivilianShips_peacefulShips_returnNotEmptyShips(){
        testShips.add(new Spaceship("1", 0, 10, 10));
        testShips.add(new Spaceship("2", 1, 10, 10));
        testShips.add(new Spaceship("3", 0, 10, 10));
        testShips.add(new Spaceship("4", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        if(result.size() == 0){
            flag=false;
        }

        for(Spaceship spaceship: result){
            if(spaceship.getFirePower() != 0){
                flag=false;
            }
        }
        Assertions.assertTrue(flag);
    }
    @DisplayName("Вернуть нул,если мирных корабликов нет")
    @Test
    void getAllCivilianShips_peacefulShips_returnEmptyShips(){
        testShips.add(new Spaceship("10", 1, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 1, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        Assertions.assertEquals(result.size(),0);
    }
}