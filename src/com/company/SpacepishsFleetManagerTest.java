package com.company;
import java.util.ArrayList;

class SpaceshipFleetManagerTest {

    SpaceshipFleetManager center;
    ArrayList<Spaceship> testShips = new ArrayList<>();


    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        boolean test1 = spaceshipFleetManagerTest.getAllCivilianShips_peacefulShips_returnNotEmptyShips();
        boolean test2 = spaceshipFleetManagerTest.getAllCivilianShips_peacefulShips_returnEmptyShips();
        boolean test3=spaceshipFleetManagerTest.getShipByName_shipName_returnShipName();
        boolean test4=spaceshipFleetManagerTest.getShip_NotExist_sayNull();
        boolean test5= spaceshipFleetManagerTest.getFirePower_BiggestPower_returnPowerfullShip();
        boolean test6=spaceshipFleetManagerTest.getFirePower_BiggestPower_returnPowerFullShipFirstInList();
        boolean test7=spaceshipFleetManagerTest.getFirePower_NotBiggestPower_returnNull();
        boolean test8= spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipsListWithEnoughCargoSpace_returnListWithShips();
        boolean test9=spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnEmptyList();
        ArrayList<Boolean> tests = new ArrayList<>();
        tests.add(test1);
        tests.add(test2);
        tests.add(test3);
        tests.add(test4);
        tests.add(test5);
        tests.add(test6);
        tests.add(test7);
        tests.add(test8);
        tests.add(test9);

        printBalls(tests);
    }





    //3 тест
    private boolean getAllCivilianShips_peacefulShips_returnNotEmptyShips(){
        testShips.add(new Spaceship("1", 0, 10, 10));
        testShips.add(new Spaceship("2", 1, 10, 10));
        testShips.add(new Spaceship("3", 0, 10, 10));
        testShips.add(new Spaceship("4", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        if(result.size() == 0){
            return false;
        }

        for(Spaceship spaceship: result){
            if(spaceship.getFirePower() != 0){
                testShips.clear();
                return false;
            }
        }
        testShips.clear();
        return true;
    }

    private boolean getAllCivilianShips_peacefulShips_returnEmptyShips(){
        testShips.add(new Spaceship("10", 1, 10, 10));
        testShips.add(new Spaceship("11", 1, 10, 10));
        testShips.add(new Spaceship("12", 1, 10, 10));
        testShips.add(new Spaceship("13", 1, 10, 10));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testShips);

        if(result.size() != 0){
            testShips.clear();
            return false;
        }
        testShips.clear();
        return true;
    }
    private boolean getShipByName_shipName_returnShipName(){
        testShips.add(new Spaceship("Big",1,1,1));
        testShips.add(new Spaceship("Small",1,1,1));
        testShips.add(new Spaceship("Mid",1,1,1));
String name = "Mid";
Spaceship result = center.getShipByName(testShips,name);
if(result!=null && result.getName().equals(name)){
    testShips.clear();
    return true;
}
testShips.clear();
        return false;
    }
    private boolean getShip_NotExist_sayNull(){
        testShips.add(new Spaceship("Big",1,1,1));
        testShips.add(new Spaceship("Small",1,1,1));
        testShips.add(new Spaceship("Mid",1,1,1));
        String name = "Mid123";
        Spaceship result = center.getShipByName(testShips,name);
        if(result==null){
            testShips.clear();
            return true;
        }
        testShips.clear();
        return false;
    }
    private boolean getFirePower_BiggestPower_returnPowerfullShip(){
        testShips.add(new Spaceship("Big",10,1,1));
        testShips.add(new Spaceship("Small",0,1,1));
        testShips.add(new Spaceship("Mid",0,1,1));
        Spaceship result=center.getMostPowerfulShip(testShips);
        if(result.getFirePower()==10 ){
            testShips.clear();
            return true;
        }
        testShips.clear();
        return false;
    }
    private boolean getFirePower_BiggestPower_returnPowerFullShipFirstInList(){
        int testPower=10;
        String testName="Test ship";
        testShips.add(new Spaceship(testName,testPower,1,1));
        testShips.add(new Spaceship("big",10,1,1));
        testShips.add(new Spaceship("Small",10,1,1));
        testShips.add(new Spaceship("Mid",0,1,1));
        Spaceship result=center.getMostPowerfulShip(testShips);
        if(result.getFirePower()==10 &&result.getName().equals(testName) ){
            testShips.clear();
            return true;
        }
        testShips.clear();
        return false;
    }

    private boolean getFirePower_NotBiggestPower_returnNull(){
        testShips.add(new Spaceship("Big",0,1,1));
        testShips.add(new Spaceship("Small",-1,1,1));
        testShips.add(new Spaceship("Mid",-1,1,1));
        Spaceship result=center.getMostPowerfulShip(testShips);
        if(result==null){
            testShips.clear();
            return true;
        }
        testShips.clear();
        return false;


    }
    private boolean getAllShipsWithEnoughCargoSpace_shipsListWithEnoughCargoSpace_returnListWithShips(){
        testShips.add(new Spaceship("Big",0,5,1));
        testShips.add(new Spaceship("Small",-1,9,1));
        testShips.add(new Spaceship("Mid",-1,10,1));
        int testCargo=10;
        ArrayList<Spaceship> testCargoSpaceships=center.getAllShipsWithEnoughCargoSpace(testShips,testCargo);
        if(testCargoSpaceships.size()==0){
            testShips.clear();
            return false;
        }
        for (int i = 0; i < testCargoSpaceships.size(); i++) {
            if(testCargoSpaceships.get(i).getCargoSpace()<testCargo){
                testShips.clear();
                return false;
            }
        }
        testShips.clear();
        return true;
    }
    private boolean getAllShipsWithEnoughCargoSpace_shipListWithEnoughCargoSpace_returnEmptyList(){
        testShips.add(new Spaceship("Big",0,9,1));
        testShips.add(new Spaceship("Small",-1,8,1));
        testShips.add(new Spaceship("Mid",-1,10,1));
        int testCargo=1320;
        ArrayList<Spaceship> testCargoSpaceships=center.getAllShipsWithEnoughCargoSpace(testShips,testCargo);
        if(testCargoSpaceships.size()==0){
            testShips.clear();
            return true;
        }
        testShips.clear();
        return false;
    }
    private static void printBalls(ArrayList<Boolean> testList){
        double balls=0;
        for (int i = 0; i <testList.size() ; i++) {
            if(testList.get(i)){
                System.out.println("All good! " + " Test " + i + " is complete");
                balls=balls+0.45;
            }else{
                System.out.println("Test " + i + " failed");
            }
        }
        System.out.printf("Your balls are " + balls);
    }



    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
}